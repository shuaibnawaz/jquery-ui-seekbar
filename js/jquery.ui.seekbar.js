(function($){
    $.widget('ui.seekbar', {
        options : {
            position: 0.0,
            buffered: 0.0,
            buffering: false,
            label1: '0%',
            label2: '100%',
            labelTemplate: '%label1 of %label2',
        },
        _create : function(){
            var self    = this,
                o       = self.options,
                el      = self.element;
            self.seekbar = {};
            self.seekbar.elements = {};
            self.seekbar.elements.track   = { el: $('<div>') };
            self.seekbar.elements.played  = { el: $('<div>') };
            self.seekbar.elements.buffered = { el: $('<div>') };
            self.seekbar.elements.gripper = { el: $('<div>') };
            self.seekbar.elements.label = { el : $('<div>') };

            var $track = self.seekbar.elements.track.el;
            var $played = self.seekbar.elements.played.el;
            var $buffered = self.seekbar.elements.buffered.el;
            var $gripper = self.seekbar.elements.gripper.el;
            var $label = self.seekbar.elements.label.el;

            $track.addClass('seekbar-track');
            $played.addClass('seekbar-played');
            $buffered.addClass('seekbar-buffered');
            $gripper.addClass('seekbar-gripper').draggable({
                axis: 'x',
                containment: "parent",
                drag: function(event, ui){
                    var gripperOffset = $(this).position();
                    var gipperWidth = $(this).outerWidth();
                    a = gripperOffset.left;
                    b = self.seekbar.elements.track.dimention.width - (parseInt(self.seekbar.elements.track.el.css('border-left-width')) + parseInt(self.seekbar.elements.track.el.css('border-right-width')) + gipperWidth);
                    var percentage = (a/b*100).toFixed(2);
                    o.position = percentage;
                    self._render();
                    self._trigger('_seeking', event, o);
                },
                stop: function(event, ui){
                    self._trigger('_seeked', event, o);
                }
            }).css('position', 'absolute');

            $track.append( $gripper, $played, $buffered );
            $(el).html( $track );
            $(el).append($label);

            $(window).load(function(){
                self.seekbar.elements.track.dimention = { width: $track.outerWidth() , height: $track.outerHeight() };
            });

            $track.click(function(event){
                var gipperWidth = self.seekbar.elements.gripper.el.outerWidth();
                var trackWidth = self.seekbar.elements.track.dimention.width - (parseInt(self.seekbar.elements.track.el.css('border-left-width')) + parseInt(self.seekbar.elements.track.el.css('border-right-width')) + gipperWidth);
                var selection = event.offsetX - Math.round(gipperWidth/2);

                if ( selection < 0 )
                    selection = 0;
                else if (selection > trackWidth)
                    selection = trackWidth;
                self.options.position = (selection/trackWidth*100).toFixed(2);
                self._render();
                self._trigger('_seeked', event, o);
            });

            self._render();
        },
        _render: function(){
            var self = this;
            var o = self.options;
            var gipperWidth = self.seekbar.elements.gripper.el.outerWidth();
            var trackWidth = self.seekbar.elements.track.el.width();
            var seekbarProjectionSize = trackWidth - (parseInt(self.seekbar.elements.track.el.css('border-left-width')) + parseInt(self.seekbar.elements.track.el.css('border-right-width')) + gipperWidth);
            var gripperAbsPos = o.position * seekbarProjectionSize / 100;
            self.seekbar.elements.gripper.el.css('left', gripperAbsPos + "px");
            self.seekbar.elements.played.el.css('width', (gripperAbsPos + Math.round(gipperWidth/2)) + "px");
            self.seekbar.elements.buffered.el.css('width', (o.buffered * seekbarProjectionSize / 100) + "px");
            var template = o.labelTemplate;
            template = template.replace('%label1', o.label1);
            template = template.replace('%label2', o.label2);

            if( typeof o.buffering != 'undefined' && o.buffering )
                self.seekbar.elements.track.el.addClass('buffering');
            else
                self.seekbar.elements.track.el.removeClass('buffering');

            self.seekbar.elements.label.el.html(template);
        },
        _setOption: function(option, value){
            $.Widget.prototype._setOption.apply( this, arguments );
            var self = this;
            var o = self.options;

            o[option] = value;

            self._render();
        },
        setOptions: function(_options){
            var self = this,
                o = self.options;

            $.extend(o, _options);
        },
        refresh: function(){
            var self = this;
            self._render();
        },
        destroy : function(){
            var self = this;
            for (e in self.seekbar.elements){
                self.seekbar.elements[e].el.remove();
            }
            delete self.seekbar;
        }
    });
})(jQuery);
